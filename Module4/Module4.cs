﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello");            
        }


        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            return array.Max();
        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0 )
                throw new ArgumentNullException("array");
            return array.Min();
        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            return array.Sum();
        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            return array.Max() - array.Min();
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            int max = array.Max();
            int min = array.Min();
            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0) array[i] += max;
                else array[i] -= min;
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            if (a.Length > b.Length)
            {
                for (int i = 0; i < b.Length; i++)
                {

                    a[i] += b[i];
                }
                return a;
            }
            else
            {
                for (int i = 0; i < a.Length; i++)
                {
                    b[i] += a[i];

                }
                return b;
            }
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
                throw new ArgumentException("radius");
            length = 2 * Math.PI * radius;
            square = radius * radius * Math.PI;
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem = array.Max();
            minItem = array.Min();
            sumOfItems = array.Sum();
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
                throw new ArgumentException("radius");
            (double, double) result = (2 * Math.PI * radius, radius * radius * Math.PI);
            return result;
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            return (array.Min(), array.Max(), array.Sum());
        }

        public void Task_5(int[] array)
        {
            if (array == null ||array.Length == 0 )
                throw new ArgumentNullException("array");
            for (int i = 0; i < array.Length; i++)
            { array[i] += 5; }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            switch (direction)
            {
                case SortDirection.Ascending: Array.Sort(array);  break;
                case SortDirection.Descending: Array.Sort(array);  Array.Reverse(array); break;
            }

        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            result = (x1 + x2) / 2.0;
            if (func(x1) * func(result) < 0)
            { x2 = result; }
            else
            { x1 = result; }
            if (Math.Abs(x2 - x1) > 2.0 * e)
                result = Task_7(func, x1, x2, e, result);            
            return result;
        }
    }
}
